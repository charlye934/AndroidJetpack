# EJEMPLOS DATA BINDING AND VIEW BINDING

View binding is only a replacement for findViewById. If you also want to automatically bind views in XML you can use the data binding library. Both libraries can be applied to the same module and they’ll work together.
When both are enabled, layouts that use a <layout> tag will use data binding to generate binding objects. All other layouts will use view binding to generate binding objects.

## VIEW BINDING
[LINK]: https://developer.android.com/topic/libraries/view-binding

You can use the binding class whenever you inflate layouts such as Fragment, Activity, or even a RecyclerView Adapter (or ViewHolder).

### Configuration
```gradle
// Available in Android Gradle Plugin 3.6.0
android {
        ...
        viewBinding {
            enabled = true
        }
    }

// Android Studio 4.0
android {
    buildFeatures {
        viewBinding = true
    }
}
```

## Vinculacion de vistas en Activity
- Llama al método inflate() estático incluido en la clase de vinculación generada. Esto crea una instancia de la clase de vinculación para la actividad que se usará.
- Para obtener una referencia a la vista raíz, llama al método getRoot() o usa la sintaxis de la propiedad Kotlin.
- Pasa la vista raíz a setContentView() para que sea la vista activa en la pantalla.
```kotlin
    private lateinit var binding: NameActivityBinding

    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        binding = NameActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }
```
Ahora puedes usar la instancia de la clase de vinculación para hacer referencia a cualquiera de las vistas:
```kotlin
    binding.name.text = viewModel.name
    binding.button.setOnClickListener { viewModel.userClicked() }
```

## Vinculacion de vistas en Fragmentos
- Llama al método inflate() estático incluido en la clase de vinculación generada. Esto crea una instancia de la clase de vinculación para que la use el fragmento.
- Para obtener una referencia a la vista raíz, llama al método getRoot() o usa la sintaxis de la propiedad Kotlin.
- Muestra la vista raíz del método onCreateView() para convertirla en la vista activa de la pantalla.
```kotlin
    private var _binding: ResultProfileBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {
        _binding = ResultProfileBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
```

## Example in RecyclerView
```kotlin
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        val binding = ItemNameBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

    class ViewHolder(private val binding: ItemNameBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(object: Object){
            binding.elment = object
        }
    }
```

Nota: Los fragmentos sobreviven a sus vistas. Asegúrate de borrar las referencias a la instancia de clase de vinculación que se encuentran en el método onDestroyView() del fragmento.

On each binding class, view binding exposes three public static functions to create a binding an object, here’s a quick guide for when to use each:
    - inflate(inflater) – Use this in an Activity onCreate where there is no parent view to pass to the binding object.
    - inflate(inflater, parent, attachToParent) – Use this in a Fragment or a RecyclerView Adapter (or ViewHolder) where you need to pass the parent ViewGroup to the binding object.
    - bind(rootView) – Use this when you’ve already inflated the view and you just want to use view binding to avoid findViewById. This is useful for fitting view binding into your existing infrastructure and when refactoring code to use ViewBinding.


